## Stockito

Stockito cover several topics: 

- Functional programing
- Reactive types
- Router Functions
- Creation on Reactive Java Services/Components
- Spring5
- Spring boot
- Mongo
- Lombok
- TDD
- Junit5
- Mockito
- Monitoring
- Prometheus
- Grafana
- Docker
- Docker Compose
- Cloud hosting - Linode - https://www.linode.com/

## usage

To run tests:

```shell
$ mvn test
```

To run this service:

```shell
$ mvn spring-boot:run
```

To build these services:

```shell
$ sudo docker-compose build & sudo docker-compose up -d
```


## Sample requests

Get all products
```shell
$ curl -X GET "http://172.104.239.180/api/products" -H  "accept: application/json"
```
```json
[ 
   { 
      "productId":"5a8a96afb3cb1f000173ceaf",
      "requestTimestamp":null,
      "stock":{ 
         "id":"5a8a96afb3cb1f000173ceae",
         "timestamp":1519031983.415000000,
         "quantity":10
      }
   },
   { 
      "productId":"5a8a96afb3cb1f000173ceb1",
      "requestTimestamp":null,
      "stock":{
         "id":"5a8a96afb3cb1f000173ceb0",
         "timestamp":1519031983.560000000,
         "quantity":120
      }
   }
]
```

Get product
```shell
$  curl -X GET http://172.104.239.180/api/stock?productId=5a8a96afb3cb1f000173ceb1
```
```json
{
  "productId": "5a8a96afb3cb1f000173ceb1",
  "requestTimestamp": 1519035064.118052,
  "stock": {
    "id": "5a8a96afb3cb1f000173ceb0",
    "timestamp": 1519031983.56,
    "quantity": 120
  }
}
```
Update stock
```shell
$  curl -v -H "Content-Type: application/json" -XPOST -d '{"id": "5a8a96afb3cb1f000173ceb0", "productId": "5a8a96afb3cb1f000173ceb1", "timestamp": "2018-07-16T22:54:01.754Z", "quantity": "32"}' http://172.104.239.180/api/updateStock
```
```json
{}
```

Get statistics
```shell
$  curl -X GET http://172.104.239.180/api/statistics?time=TODAY
```
```json
{
  "id": "5a8a96afb3cb1f000173ceb2",
  "topAvailableProducts": [
    {
      "productId": "1",
      "timestamp": 1519031983.573,
      "quantity": 10
    },
    {
      "productId": "2",
      "timestamp": 1519031983.573,
      "quantity": 9
    },
    {
      "productId": "3",
      "timestamp": 1519031983.573,
      "quantity": 8
    }
  ],
  "topSelingProducts": [
    {
      "productId": "4",
      "itemSold": 20
    },
    {
      "productId": "5",
      "itemSold": 19
    },
    {
      "productId": "6",
      "itemSold": 18
    }
  ],
  "range": "TODAY",
  "requestTimestamp": 1519036339.245024
}
```

```shell
$  curl -X GET http://172.104.239.180/api/statistics?time=LASTMONTH
```

```json
{
  "id": "5a8a96afb3cb1f000173ceb3",
  "topAvailableProducts": [
    {
      "productId": "1",
      "timestamp": 1519031983.599,
      "quantity": 100
    },
    {
      "productId": "2",
      "timestamp": 1519031983.599,
      "quantity": 90
    },
    {
      "productId": "3",
      "timestamp": 1519031983.599,
      "quantity": 80
    }
  ],
  "topSelingProducts": [
    {
      "productId": "4",
      "itemSold": 200
    },
    {
      "productId": "5",
      "itemSold": 190
    },
    {
      "productId": "6",
      "itemSold": 180
    }
  ],
  "range": "LASTMONTH",
  "requestTimestamp": 1519036414.048958
}
```

## Monitoring
Grafana: http://172.104.239.180:3000

User: admin
Pass:admin

[Graphs from docker containers](http://172.104.239.180:3000/dashboard/db/docker-containers?refresh=10s&orgId=1)

[Graphs from docker host](http://172.104.239.180:3000/dashboard/db/docker-host?refresh=10s&orgId=1)

[Graphs from monitor service](http://172.104.239.180:3000/dashboard/db/monitor-services?refresh=10s&orgId=1)


## Project Structure
- [stockito-api]
	 - [main/java]
	    - [/routers] : Reactive routing functions.
	    - [/handlers] : Handlers used by the routers.
	    - [/services] : Services for the business logic needed by handlers.
	    - [/repository] : Respository from POJOs.
	    - [/model]: POJOs.
	 - [test/java]
	    - [/integrationtests] : Integration tests.
	    - [/handlers] : Unit tests for handlers.
	    - [/services] : Unit tests for services.
	    - [/repository] : Unit test for repository. 
- [grafana] - confs of dashboard and datasources, setup.sh the script of import of confs.
- [prometheus] - confs of the prometheus.

package com.hans.stockito;

import com.hans.stockito.repository.ProductRepository;
import com.hans.stockito.repository.SalesHistoryRepository;
import com.hans.stockito.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.config.EnableWebFlux;

import javax.annotation.PostConstruct;

@SpringBootApplication
@EnableWebFlux
public class StockitoApplication {
    public static void main(String[] args) {
        SpringApplication.run(StockitoApplication.class, args);
    }
}

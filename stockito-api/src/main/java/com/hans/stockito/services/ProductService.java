package com.hans.stockito.services;

import com.hans.stockito.model.Product;
import com.hans.stockito.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public Mono<Product> get(String id) {
        return productRepository
                .findById(id)
                .flatMap(product -> {
                    product.setRequestTimestamp(Instant.now());
                    return Mono.just(product);
                })
                .switchIfEmpty(Mono.empty());
    }

    public Flux<Product> list() {
        return productRepository.findAll();
    }
}

package com.hans.stockito.services;

import com.hans.stockito.model.SalesHistory;
import com.hans.stockito.repository.SalesHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.Instant;

@Service
public class StatisticsService {

    @Autowired
    private SalesHistoryRepository salesHistoryRepository;

    public Mono<SalesHistory> get(String rage) {
        return salesHistoryRepository
                .findByRange(rage)
                .flatMap(salesHistory -> {
                    salesHistory.setRequestTimestamp(Instant.now());
                    return Mono.just(salesHistory);
                })
                .switchIfEmpty(Mono.empty());
    }
}

package com.hans.stockito.services;

import com.hans.stockito.model.Product;
import com.hans.stockito.model.Stock;
import com.hans.stockito.model.json.StockUpdateJsonInput;
import com.hans.stockito.repository.ProductRepository;
import com.hans.stockito.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class StockService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private StockRepository stockRepository;

    public Mono<Product> update(StockUpdateJsonInput stockUpdateJsonInput) {
        if (!productRepository.existsById(stockUpdateJsonInput.getProductId()).block())
            return Mono.empty();
        if (!stockRepository.existsById(stockUpdateJsonInput.getId()).block())
            return Mono.empty();
        Stock stock = stockRepository.save(stockUpdateJsonInput.toStock()).block();
        Product product = Product.builder().productId(stockUpdateJsonInput.getProductId()).stock(stock).build();
        return Mono.just(productRepository.save(product).block());
    }
}

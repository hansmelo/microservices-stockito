package com.hans.stockito.handlers;

import com.hans.stockito.model.json.StockUpdateJsonInput;
import com.hans.stockito.services.ProductService;
import com.hans.stockito.services.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

@Component
public class StockHandler {

    public static final String QUERY_PARAM_PRODUCT_ID = "productId";

    @Autowired
    private ProductService productService;

    @Autowired
    private StockService stockService;

    public Mono<ServerResponse> getProduct(ServerRequest request) {
        Optional<String> productId = request.queryParam(QUERY_PARAM_PRODUCT_ID);
        if (!productId.isPresent()) return ServerResponse.badRequest().build();

        return productService.get(productId.get())
                .flatMap(product -> ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromObject(product)))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> updateStock(ServerRequest request) {
        Mono<StockUpdateJsonInput> stockInput = request.bodyToMono(StockUpdateJsonInput.class);
        StockUpdateJsonInput jsonInput = stockInput.block();
        //if (jsonInput == null || !jsonInput.isValid()) return ServerResponse.badRequest().build();
        URI createdPath;
        try {
            createdPath = new URI("/stock?productId=" + jsonInput.getProductId());
        } catch (URISyntaxException ex) {
            return ServerResponse.unprocessableEntity().build();
        }
        return stockService.update(jsonInput)
                .flatMap(stockInputUpdate -> ServerResponse.created(createdPath).contentType(MediaType.APPLICATION_JSON).build())
                .switchIfEmpty(ServerResponse.notFound().build());
    }
}

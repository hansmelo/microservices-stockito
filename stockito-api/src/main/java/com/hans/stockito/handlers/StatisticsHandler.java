package com.hans.stockito.handlers;

import com.hans.stockito.services.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.Optional;
import java.util.Set;

@Component
public class StatisticsHandler {

    public static final String QUERY_PARAM_RANGE = "time";
    public static final Set<String> ranges = Set.of("TODAY", "LASTMONTH");
    @Autowired
    private StatisticsService statisticsService;

    public Mono<ServerResponse> getSalesHistory(ServerRequest request) {
        Optional<String> range = request.queryParam(QUERY_PARAM_RANGE);
        if (!range.isPresent() || !ranges.contains(range.get())) return ServerResponse.badRequest().build();

        return statisticsService.get(range.get())
                .flatMap(salesHistory -> ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromObject(salesHistory)))
                .switchIfEmpty(ServerResponse.notFound().build());
    }
}

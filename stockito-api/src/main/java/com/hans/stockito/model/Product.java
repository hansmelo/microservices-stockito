package com.hans.stockito.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@Document
public class Product {
    @Id
    private String productId;
    @Transient
    private Instant requestTimestamp;
    private Stock stock;
}
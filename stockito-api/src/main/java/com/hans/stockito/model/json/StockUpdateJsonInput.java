package com.hans.stockito.model.json;

import com.hans.stockito.model.Stock;
import lombok.*;

import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class StockUpdateJsonInput {
    private String id;
    private Instant timestamp;
    private String productId;
    private int quantity;

    public Stock toStock() {
        return Stock.builder().id(this.id).quantity(this.quantity).timestamp(this.timestamp).build();
    }

    public Boolean isValid() {
        return this.getId() != null && this.getProductId() != null && this.getTimestamp()  != null && this.getQuantity() >= 0;
    }
}

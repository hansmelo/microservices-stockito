package com.hans.stockito.model;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class TopSelingProduct implements Comparable<TopSelingProduct> {
    private String productId;
    private int itemSold;

    @Override
    public int compareTo(TopSelingProduct topSelingProduct) {
        return topSelingProduct.getItemSold() - this.itemSold;
    }
}

package com.hans.stockito.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.SortedSet;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@Document
public class SalesHistory {
    @Id
    private String id;
    private SortedSet<TopAvalibleProduct> topAvailableProducts;
    private SortedSet<TopSelingProduct> topSelingProducts;
    @Indexed(unique=true)
    private String range;
    @Transient
    private Instant requestTimestamp;

}

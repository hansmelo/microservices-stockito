package com.hans.stockito.model;


import lombok.*;

import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class TopAvalibleProduct implements Comparable<TopAvalibleProduct> {
    private String productId;
    private Instant timestamp;
    private int quantity;

    @Override
    public int compareTo(TopAvalibleProduct topAvalibleProduct) {
        return topAvalibleProduct.getQuantity() - this.quantity;
    }
}

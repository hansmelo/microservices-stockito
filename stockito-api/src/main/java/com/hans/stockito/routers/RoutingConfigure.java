package com.hans.stockito.routers;

import com.hans.stockito.handlers.ProductHandler;
import com.hans.stockito.handlers.StatisticsHandler;
import com.hans.stockito.handlers.StockHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class RoutingConfigure {

    private static final String API_PATH = "/api";
    private static final String STOCK_PATH = "/stock";
    private static final String UPDATE_STOCK_PATH = API_PATH + "/updateStock";
    private static final String GET_PRODUCT_PATH = API_PATH + STOCK_PATH;
    private static final String STATISTICS_PATH = "/statistics";
    private static final String GET_STATISTICS_PATH = API_PATH + STATISTICS_PATH;
    private static final String LIST_PRODUCT_PATH = API_PATH + "/products";
    @Autowired
    private StockHandler stockHandler;
    @Autowired
    private StatisticsHandler statisticsHandler;
    @Autowired
    private ProductHandler productHandler;

    @Bean
    public RouterFunction<?> doRoute() {
        return route(GET(GET_PRODUCT_PATH).and(accept(MediaType.APPLICATION_PROBLEM_JSON)), stockHandler::getProduct)
                .andRoute(GET(LIST_PRODUCT_PATH).and(accept(MediaType.APPLICATION_JSON)), productHandler::listProducts)
                .andRoute(POST(UPDATE_STOCK_PATH).and(contentType(MediaType.APPLICATION_JSON)), stockHandler::updateStock)
                .andRoute(GET(GET_STATISTICS_PATH).and(accept(MediaType.APPLICATION_JSON)), statisticsHandler::getSalesHistory);
    }
}

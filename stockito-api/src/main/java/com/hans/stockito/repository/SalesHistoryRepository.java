package com.hans.stockito.repository;

import com.hans.stockito.model.SalesHistory;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface SalesHistoryRepository extends ReactiveMongoRepository<SalesHistory, String> {
    Mono<SalesHistory> findByRange(String range);
}

package com.hans.stockito.repository;

import com.hans.stockito.model.Stock;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

import java.time.Instant;

public interface StockRepository extends ReactiveMongoRepository<Stock, String> {

    Mono<Stock> findByIdAndTimestampBefore(String id, Instant instant);
}

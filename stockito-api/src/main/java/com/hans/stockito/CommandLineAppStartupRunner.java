package com.hans.stockito;

import com.hans.stockito.model.*;
import com.hans.stockito.repository.ProductRepository;
import com.hans.stockito.repository.SalesHistoryRepository;
import com.hans.stockito.repository.StockRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.SortedSet;
import java.util.TreeSet;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {

    @Autowired
    StockRepository stockRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    SalesHistoryRepository salesHistoryRepository;

    private static final Logger logger = LoggerFactory.getLogger(CommandLineAppStartupRunner.class);

    @Override
    public void run(String...args) throws Exception {
        try {

            stockRepository.deleteAll().block();
            productRepository.deleteAll().block();
            salesHistoryRepository.deleteAll().block();

            Stock stock = Stock.builder().quantity(10).timestamp(Instant.now()).build();
            stockRepository.save(stock).flatMap(stockSave -> productRepository.save(Product.builder().stock(stockSave).build())).block();
            Stock stock2 = Stock.builder().quantity(120).timestamp(Instant.now()).build();
            stockRepository.save(stock2).flatMap(stockSave -> productRepository.save(Product.builder().stock(stockSave).build())).block();


            SortedSet<TopAvalibleProduct> topAvalibleProducts = new TreeSet();
            topAvalibleProducts.add(TopAvalibleProduct.builder().productId("1").quantity(10).timestamp(Instant.now()).build());
            topAvalibleProducts.add(TopAvalibleProduct.builder().productId("2").quantity(9).timestamp(Instant.now()).build());
            topAvalibleProducts.add(TopAvalibleProduct.builder().productId("3").quantity(8).timestamp(Instant.now()).build());

            SortedSet<TopSelingProduct> topSelingProducts = new TreeSet();
            topSelingProducts.add(TopSelingProduct.builder().productId("4").itemSold(20).build());
            topSelingProducts.add(TopSelingProduct.builder().productId("5").itemSold(19).build());
            topSelingProducts.add(TopSelingProduct.builder().productId("6").itemSold(18).build());

            SalesHistory salesHistory = SalesHistory.builder()
                    .topAvailableProducts(topAvalibleProducts)
                    .topSelingProducts(topSelingProducts)
                    .range("TODAY")
                    .build();

            salesHistoryRepository.save(salesHistory).block();

            SortedSet<TopAvalibleProduct> topAvalibleProductsMonth = new TreeSet();
            topAvalibleProductsMonth.add(TopAvalibleProduct.builder().productId("1").quantity(100).timestamp(Instant.now()).build());
            topAvalibleProductsMonth.add(TopAvalibleProduct.builder().productId("2").quantity(90).timestamp(Instant.now()).build());
            topAvalibleProductsMonth.add(TopAvalibleProduct.builder().productId("3").quantity(80).timestamp(Instant.now()).build());

            SortedSet<TopSelingProduct> topSelingProductsMonth = new TreeSet();
            topSelingProductsMonth.add(TopSelingProduct.builder().productId("4").itemSold(200).build());
            topSelingProductsMonth.add(TopSelingProduct.builder().productId("5").itemSold(190).build());
            topSelingProductsMonth.add(TopSelingProduct.builder().productId("6").itemSold(180).build());

            SalesHistory salesHistoryMonth = SalesHistory.builder()
                    .topAvailableProducts(topAvalibleProductsMonth)
                    .topSelingProducts(topSelingProductsMonth)
                    .range("LASTMONTH")
                    .build();

            salesHistoryRepository.save(salesHistoryMonth).block();


            logger.info("The data was initialized.");
        } catch (Exception ex) {

        }
    }
}
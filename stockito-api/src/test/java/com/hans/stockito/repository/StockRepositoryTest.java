package com.hans.stockito.repository;

import com.hans.stockito.model.Stock;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;

import static com.hans.stockito.integrationtests.GetProductIntegrationTest.PRODUCT_QUANTITY;
import static com.hans.stockito.integrationtests.GetProductIntegrationTest.STOCK_TIMESTAMP;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@RunWith(JUnitPlatform.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration
public class StockRepositoryTest {

    @Autowired
    private StockRepository stockRepository;

    @Test
    void crudTest() {
        Stock stock = Stock.builder().quantity(PRODUCT_QUANTITY).timestamp(STOCK_TIMESTAMP).build();

        Stock stockSaved = stockRepository.save(stock).block();

        Stock stockFromDb = stockRepository.findById(stock.getId()).block();

        stockFromDb.setQuantity(32);
        stockFromDb.setTimestamp(Instant.now());

        Stock stockUpdated = stockRepository.save(stockFromDb).block();

        assertEquals(32, stockUpdated.getQuantity());
        assertEquals(stockFromDb.getId(), stockUpdated.getId());
        assertNotEquals(stockFromDb.getQuantity(), stockSaved.getQuantity());

    }
}

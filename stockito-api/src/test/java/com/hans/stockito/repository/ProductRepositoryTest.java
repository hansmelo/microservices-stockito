package com.hans.stockito.repository;

import com.hans.stockito.model.Product;
import com.hans.stockito.model.Stock;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Mono;

import java.time.temporal.ChronoUnit;

import static com.hans.stockito.integrationtests.GetProductIntegrationTest.PRODUCT_QUANTITY;
import static com.hans.stockito.integrationtests.GetProductIntegrationTest.STOCK_TIMESTAMP;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(JUnitPlatform.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration
public class ProductRepositoryTest {

    private static final String MOCK_ID = RandomString.make();

    @Autowired
    ProductRepository productRepository;

    @MockBean
    StockRepository stockRepository;

    @Test
    void crudTest() {
        Stock stock = Stock.builder().id(MOCK_ID).quantity(PRODUCT_QUANTITY).timestamp(STOCK_TIMESTAMP).build();
        when(stockRepository.insert(stock)).thenReturn(Mono.just(stock));

        Product product = Product.builder().stock(stock).build();
        Product productSave = this.productRepository.insert(product).block();

        assertAll("Assert Saved Product",
                () -> assertNotNull(productSave.getProductId()),
                () -> assertEquals(stock.getId(), productSave.getStock().getId()),
                () -> assertEquals(stock.getQuantity(), productSave.getStock().getQuantity()),
                () -> assertEquals(stock.getTimestamp(), productSave.getStock().getTimestamp())
        );

        Product productRetrieved = this.productRepository.findById(product.getProductId()).block();

                assertAll("Assert Retrieved Product",
                () -> assertNotNull(productRetrieved.getProductId()),
                () -> assertEquals(stock.getId(), productRetrieved.getStock().getId()),
                () -> assertEquals(stock.getQuantity(), productRetrieved.getStock().getQuantity()),
                () -> assertEquals(stock.getTimestamp().truncatedTo(ChronoUnit.MILLIS), productRetrieved.getStock().getTimestamp())
        );

    }
}

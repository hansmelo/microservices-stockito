package com.hans.stockito.repository;

import com.hans.stockito.model.SalesHistory;
import com.hans.stockito.model.TopAvalibleProduct;
import com.hans.stockito.model.TopSelingProduct;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.util.SortedSet;
import java.util.TreeSet;

import static com.hans.stockito.integrationtests.GetStatisticsIntegrationTest.TODAY_RANGE;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(JUnitPlatform.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration
public class SalesHistoryRepositoryTest {

    @Autowired
    SalesHistoryRepository salesHistoryRepository;

    @Test
    void crudTest() {
        if (salesHistoryRepository.findByRange("TODAY").block() == null) {

            SortedSet<TopAvalibleProduct> topAvalibleProducts = new TreeSet();
            topAvalibleProducts.add(TopAvalibleProduct.builder().productId("1").quantity(10).timestamp(Instant.now()).build());
            topAvalibleProducts.add(TopAvalibleProduct.builder().productId("2").quantity(9).timestamp(Instant.now()).build());
            topAvalibleProducts.add(TopAvalibleProduct.builder().productId("3").quantity(8).timestamp(Instant.now()).build());

            SortedSet<TopSelingProduct> topSelingProducts = new TreeSet();
            topSelingProducts.add(TopSelingProduct.builder().productId("4").itemSold(20).build());
            topSelingProducts.add(TopSelingProduct.builder().productId("5").itemSold(19).build());
            topSelingProducts.add(TopSelingProduct.builder().productId("6").itemSold(18).build());

            SalesHistory salesHistory = SalesHistory.builder()
                    .topAvailableProducts(topAvalibleProducts)
                    .topSelingProducts(topSelingProducts)
                    .range(TODAY_RANGE)
                    .build();

            SalesHistory salesHistorySave = salesHistoryRepository.save(salesHistory).block();
            assertNotNull(salesHistorySave.getId());
        }

        SalesHistory salesHistoryRetrieved = this.salesHistoryRepository.findByRange(TODAY_RANGE).block();

        assertAll("Assert Retrivied SalesHistory",
                () -> assertNotNull(salesHistoryRetrieved.getId()),
                () -> assertEquals(TODAY_RANGE, salesHistoryRetrieved.getRange())
                );

    }
}

package com.hans.stockito.handlers;

import com.hans.stockito.model.Product;
import com.hans.stockito.model.Stock;
import com.hans.stockito.model.json.StockUpdateJsonInput;
import com.hans.stockito.services.ProductService;
import com.hans.stockito.services.StockService;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.server.ServerRequest;
import reactor.core.publisher.Mono;

import java.util.Optional;

import static com.hans.stockito.integrationtests.GetProductIntegrationTest.PRODUCT_QUANTITY;
import static com.hans.stockito.integrationtests.GetProductIntegrationTest.STOCK_TIMESTAMP;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnitPlatform.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StockHandlerTest {

    private static final String MOCK_ID = RandomString.make();

    @Autowired
    private StockHandler stockHandler;

    @MockBean
    ProductService productService;

    @MockBean
    StockService stockService;

    @Test
    void getProductTest() {
        ServerRequest serverRequest = mock(ServerRequest.class);
        when(serverRequest.queryParam(StockHandler.QUERY_PARAM_PRODUCT_ID)).thenReturn(Optional.of(MOCK_ID));
        Product productMock = mock(Product.class);
        when(productService.get(MOCK_ID)).thenReturn(Mono.just(productMock));

        stockHandler.getProduct(serverRequest).subscribe(response -> assertEquals(HttpStatus.OK, response.statusCode()));
    }

    @Test
    void updateStockTest() {
        StockUpdateJsonInput stockInput = mock(StockUpdateJsonInput.class);
        Product product = mock(Product.class);
        ServerRequest serverRequest = mock(ServerRequest.class);
        when(serverRequest.bodyToMono(StockUpdateJsonInput.class)).thenReturn(Mono.just(stockInput));
        when(stockService.update(stockInput)).thenReturn(Mono.just(product));

        stockHandler.updateStock(serverRequest).subscribe(response -> assertEquals(HttpStatus.CREATED, response.statusCode()));
    }

}

package com.hans.stockito.handlers;

import com.hans.stockito.model.SalesHistory;
import com.hans.stockito.services.StatisticsService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.server.ServerRequest;
import reactor.core.publisher.Mono;

import java.util.Optional;

import static com.hans.stockito.integrationtests.GetStatisticsIntegrationTest.TODAY_RANGE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnitPlatform.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StatisticsHandlerTest {

    @Autowired
    private StatisticsHandler statisticsHandler;

    @MockBean
    StatisticsService statisticsService;

    @Test
    void getStatistics() {
        ServerRequest serverRequest = mock(ServerRequest.class);
        when(serverRequest.queryParam(StatisticsHandler.QUERY_PARAM_RANGE)).thenReturn(Optional.of(TODAY_RANGE));
        SalesHistory salesHistory = mock(SalesHistory.class);
        when(statisticsService.get(TODAY_RANGE)).thenReturn(Mono.just(salesHistory));

        statisticsHandler.getSalesHistory(serverRequest).subscribe(response -> assertEquals(HttpStatus.OK, response.statusCode()));

    }
}

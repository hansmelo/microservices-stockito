package com.hans.stockito.integrationtests;

import com.hans.stockito.model.SalesHistory;
import com.hans.stockito.model.TopAvalibleProduct;
import com.hans.stockito.model.TopSelingProduct;
import com.hans.stockito.repository.SalesHistoryRepository;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.Instant;
import java.util.SortedSet;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(JUnitPlatform.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration
public class GetStatisticsIntegrationTest {
    public static final String GET_STATISTICS_PATH = "/api/statistics?time={time}";
    public static final String WRONG_GET_STATISTICS_PATH = "/api/statistics?timeWrong={rangeTimeWrong}";
    public static final String TODAY_RANGE = "TODAY";
    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private SalesHistoryRepository salesHistoryRepository;

    @BeforeEach
    void setup() {
        if (salesHistoryRepository.findByRange(TODAY_RANGE).block() == null) {

            SortedSet<TopAvalibleProduct> topAvalibleProducts = new TreeSet();
            topAvalibleProducts.add(TopAvalibleProduct.builder().productId("1").quantity(10).timestamp(Instant.now()).build());
            topAvalibleProducts.add(TopAvalibleProduct.builder().productId("2").quantity(9).timestamp(Instant.now()).build());
            topAvalibleProducts.add(TopAvalibleProduct.builder().productId("3").quantity(8).timestamp(Instant.now()).build());

            SortedSet<TopSelingProduct> topSelingProducts = new TreeSet();
            topSelingProducts.add(TopSelingProduct.builder().productId("4").itemSold(20).build());
            topSelingProducts.add(TopSelingProduct.builder().productId("5").itemSold(19).build());
            topSelingProducts.add(TopSelingProduct.builder().productId("6").itemSold(18).build());

            SalesHistory salesHistory = SalesHistory.builder()
                    .topAvailableProducts(topAvalibleProducts)
                    .topSelingProducts(topSelingProducts)
                    .range(TODAY_RANGE)
                    .build();

            salesHistoryRepository.save(salesHistory).block();
        }
    }

    @Test
    void getStatistics() {
        SalesHistory salesHistory = this.webTestClient.get().uri(GET_STATISTICS_PATH, TODAY_RANGE)
                .exchange().expectStatus().isOk()
                .expectBody(SalesHistory.class).returnResult().getResponseBody();

        assertAll("SalesHistory match",
                () -> assertEquals(TODAY_RANGE, salesHistory.getRange())
        );
    }

    @Test
    void getStatistics_WrongPath() {
        this.webTestClient.get().uri(WRONG_GET_STATISTICS_PATH, RandomString.make())
                .exchange().expectStatus().isBadRequest().expectBody().isEmpty();
    }

    @Test
    void getStatistics_WrongRange() {
        this.webTestClient.get().uri(GET_STATISTICS_PATH, RandomString.make())
                .exchange().expectStatus().isBadRequest().expectBody().isEmpty();
    }

}

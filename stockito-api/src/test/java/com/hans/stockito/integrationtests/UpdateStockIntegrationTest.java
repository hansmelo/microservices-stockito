package com.hans.stockito.integrationtests;

import com.hans.stockito.model.Product;
import com.hans.stockito.model.Stock;
import com.hans.stockito.model.json.StockUpdateJsonInput;
import com.hans.stockito.repository.ProductRepository;
import com.hans.stockito.repository.StockRepository;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.Instant;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

//reactor.core.Exceptions$BubblingException: java.io.IOException: Connection closed prematurely
@Ignore
@RunWith(JUnitPlatform.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration(exclude = {EmbeddedMongoAutoConfiguration.class})
public class UpdateStockIntegrationTest {


    public static final String UPDATE_STOCK_PATH = "/api/updateStock";
    public static final int PRODUCT_QUANTITY = 10;
    public static final Instant STOCK_TIMESTAMP = Instant.now();
    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private StockRepository stockRepository;

    Product productSave = null;

    @BeforeEach
    void setup() {
        Stock stock = Stock.builder().quantity(PRODUCT_QUANTITY).timestamp(STOCK_TIMESTAMP).build();
        productSave = stockRepository.save(stock)
                .flatMap(stockSave -> productRepository.save(Product.builder().stock(stockSave).build()))
                .block();
    }

    @Ignore
    void updateStock() {
        StockUpdateJsonInput stockInput = StockUpdateJsonInput.builder()
                .id(productSave.getStock().getId())
                .productId(productSave.getProductId())
                .quantity(32)
                .timestamp(Instant.now())
                .build();

        stockRepository = mock(StockRepository.class);
        productRepository = mock(ProductRepository.class);
        Stock stockMock = stockInput.toStock();
        when(productRepository.existsById(stockInput.getProductId())).thenReturn(Mono.just(true));
        when(stockRepository.existsById(stockInput.getId())).thenReturn(Mono.just(true));
        when(stockRepository.save(stockMock)).thenReturn(Mono.just(stockMock));

        this.webTestClient.post().uri(UPDATE_STOCK_PATH)
                .body(BodyInserters.fromObject(stockInput)).exchange().expectStatus().isCreated().expectBody().isEmpty();
    }
}

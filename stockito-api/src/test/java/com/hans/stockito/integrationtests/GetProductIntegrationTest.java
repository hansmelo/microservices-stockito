package com.hans.stockito.integrationtests;

import com.hans.stockito.model.Product;
import com.hans.stockito.model.Stock;
import com.hans.stockito.repository.ProductRepository;
import com.hans.stockito.repository.StockRepository;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(JUnitPlatform.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration
public class GetProductIntegrationTest {

    public static final String GET_PRODUCT_PATH = "/api/stock?productId={productId}";
    public static final String WRONG_GET_PRODUCT_PATH = "/api/stock?wrongId={wrongId}";
    public static final int PRODUCT_QUANTITY = 10;
    public static final Instant STOCK_TIMESTAMP = Instant.now();
    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private StockRepository stockRepository;

    Product productSave = null;

    @BeforeEach
    void setup() {
        Stock stock = Stock.builder().quantity(PRODUCT_QUANTITY).timestamp(STOCK_TIMESTAMP).build();
        productSave = stockRepository.save(stock)
                .flatMap(stockSave ->  productRepository.save(Product.builder().stock(stockSave).build()))
                .block();

    }

    @Test
    void getProduct() {
        Product product = this.webTestClient.get().uri(GET_PRODUCT_PATH, productSave.getProductId())
                .exchange().expectStatus().isOk()
                .expectBody(Product.class).returnResult().getResponseBody();

        assertAll("Product match",
                () -> assertEquals(productSave.getProductId(), product.getProductId()),
                () -> assertNotNull(product.getRequestTimestamp()),
                () -> assertEquals(productSave.getStock().getId(), product.getStock().getId()),
                () -> assertEquals(PRODUCT_QUANTITY, product.getStock().getQuantity()),
                () -> assertEquals(STOCK_TIMESTAMP.truncatedTo(ChronoUnit.MILLIS), product.getStock().getTimestamp())
        );
    }

    @Test
    void getProduct_NotFound() {
        this.webTestClient.get().uri(GET_PRODUCT_PATH, RandomString.make())
                .exchange().expectStatus().isNotFound().expectBody().isEmpty();

    }

    @Test
    void getProduct_WrongPath() {
        this.webTestClient.get().uri(WRONG_GET_PRODUCT_PATH, RandomString.make())
                .exchange().expectStatus().isBadRequest().expectBody().isEmpty();
    }
}

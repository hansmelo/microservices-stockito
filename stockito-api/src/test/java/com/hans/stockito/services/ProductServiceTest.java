package com.hans.stockito.services;

import com.hans.stockito.model.Product;
import com.hans.stockito.repository.ProductRepository;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Mono;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(JUnitPlatform.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductServiceTest {

    @Autowired
    ProductService productService;

    @MockBean
    ProductRepository productRepository;

    String randomId = RandomString.make();

    @Test
    void getProductTest() {
        when(productRepository.findById(randomId))
                .thenReturn(Mono.just(Product.builder().productId(randomId).build()));

        Mono<Product> productMonoFromService = productService.get(randomId);
        assertEquals(productMonoFromService.block().getProductId(), randomId);
    }
}

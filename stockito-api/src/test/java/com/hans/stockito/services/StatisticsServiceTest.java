package com.hans.stockito.services;

import com.hans.stockito.model.SalesHistory;
import com.hans.stockito.repository.SalesHistoryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Mono;

import static com.hans.stockito.integrationtests.GetStatisticsIntegrationTest.TODAY_RANGE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(JUnitPlatform.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StatisticsServiceTest {

    @Autowired
    StatisticsService statisticsService;

    @MockBean
    SalesHistoryRepository salesHistoryRepository;

    @Test
    void getStatisticsTest() {
        when(salesHistoryRepository.findByRange(TODAY_RANGE))
                .thenReturn(Mono.just(SalesHistory.builder().range(TODAY_RANGE).build()));

        Mono<SalesHistory> salesHistoryMonoFromService = statisticsService.get(TODAY_RANGE);
        assertEquals(TODAY_RANGE, salesHistoryMonoFromService.block().getRange());
    }
}

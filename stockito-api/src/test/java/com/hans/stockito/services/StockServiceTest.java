package com.hans.stockito.services;


import com.hans.stockito.model.Product;
import com.hans.stockito.model.Stock;
import com.hans.stockito.model.json.StockUpdateJsonInput;
import com.hans.stockito.repository.ProductRepository;
import com.hans.stockito.repository.StockRepository;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Mono;

import static com.hans.stockito.integrationtests.GetProductIntegrationTest.PRODUCT_QUANTITY;
import static com.hans.stockito.integrationtests.GetProductIntegrationTest.STOCK_TIMESTAMP;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(JUnitPlatform.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StockServiceTest {

    String randomId = RandomString.make();

    @Autowired
    StockService stockService;

    @MockBean
    StockRepository stockRepository;

    @MockBean
    ProductRepository productRepository;

    @Test
    void updateProductTest() {
        StockUpdateJsonInput stockInput = StockUpdateJsonInput.builder()
                .id(randomId)
                .productId(randomId)
                .quantity(PRODUCT_QUANTITY)
                .timestamp(STOCK_TIMESTAMP)
                .build();

        Stock stockMock = stockInput.toStock();
        Product productMock = Product.builder().productId(stockInput.getProductId()).stock(stockMock).build();
        when(productRepository.existsById(stockInput.getProductId())).thenReturn(Mono.just(true));
        when(stockRepository.existsById(stockInput.getId())).thenReturn(Mono.just(true));
        when(stockRepository.save(stockMock)).thenReturn(Mono.just(stockMock));
        when(productRepository.save(productMock)).thenReturn(Mono.just(productMock));

        Mono<Product> productUpdateFromService = stockService.update(stockInput);
        assertEquals(productUpdateFromService.block().getStock().getId(), stockInput.getId());

    }
}
